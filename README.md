# Micro Blog Feed

Basic project to integrate RSS, Twitter, and Discord feeds into ReactJS websites

## Project Goals:
* Develop an NPM module which can be installed into a ReactJS/Web or WebApp project that processes RSS feeds from other services such as Twitter, Discord. or Mastodon.
* Front-end module will leverage a sentiment analysis back-end. 
* Independent sub-project will aggregate DenoJS-based lambda network to aggregate RSS feeds including filtering and forwarding via open domain machine learning system.

## Proposed Technologies:
* [TailWindCSS](https://github.com/tailwindlabs/tailwindcss)
* [ReactJS](https://github.com/reactjs/reactjs.org)
* [Quick-Feed](https://github.com/amcquade/quick-feed)
